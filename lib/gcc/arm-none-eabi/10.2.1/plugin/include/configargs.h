/* Generated automatically. */
static const char configuration_arguments[] = "/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/src/gcc/configure --target=arm-none-eabi --prefix=/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/build-arm-none-eabi/install// --with-gmp=/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/build-arm-none-eabi/host-tools --with-mpfr=/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/build-arm-none-eabi/host-tools --with-mpc=/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/build-arm-none-eabi/host-tools --with-isl=/tmp/dgboter/bbs/build09--cen7x86_64/buildbot/cen7x86_64--arm-none-eabi--a-profile/build/build-arm-none-eabi/host-tools --disable-shared --disable-nls --disable-threads --disable-tls --enable-checking=release --enable-languages=c,c++,fortran --with-newlib --with-multilib-list=aprofile --with-pkgversion='GNU Toolchain for the A-profile Architecture 10.2-2020.11 (arm-10.16)' --with-bugurl=https://bugs.linaro.org/";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "arm7tdmi" }, { "float", "soft" } };
